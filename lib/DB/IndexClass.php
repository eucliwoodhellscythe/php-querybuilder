<?

namespace DB; 

class IndexClass
{
	public function run()
	{
		$config = array();

		require '/config/local.config.php';

		$db = new QueryBuilder($config);

		/*
		@Select
		$db
		   ->select(['id', 'value_d1'])
		   ->from('test')
		   ->where([QueryBuilder::IS_NOT_NULL, 'value_d1']);

		$db->execute()->fetchAll();

		@Insert
		$db
		   ->insert(['value_d1' => 'test1', 'value_d2' => 'test2', 'value_d3' => 'test3'])
		   ->into('test');

		$db->execute();

		@Update
		$db
		   ->update('test')
		   ->set(['value_d1' => 'test15', 'value_d2' => 'test8'])
		   ->where(['id' => '6']);

		$db->execute();

		@Delete
		$db
		   ->delete('test')
		   ->where(['id' => 'test']);

		$db->execute();
		*/

		$db->rawSql();
	}
}