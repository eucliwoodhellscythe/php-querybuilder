<?

namespace DB\Helper;

class ExpressionBuilder
{
	const TYPE_AND = 'AND',
		  TYPE_OR = 'OR';

	protected static $type;

	public static function setType($type)
	{
		self::$type = $type;
	}

	public static function notLike($whFiled, $whValue = null)
	{
		return self::construct($whFiled, 'NOT LIKE', (!is_null($whValue) ? $whValue : ''));
	}

	public static function like($whFiled, $whValue = null)
	{
		return self::construct($whFiled, 'LIKE', (!is_null($whValue) ? $whValue : ''));
	}

	public static function in($whFiled, $whValue)
	{
		return self::construct($whFiled, 'IN', '(' . implode(', ', array_fill(0, count($whValue), '?')) . ')');
	}

	public static function notin($whFiled, $whValue)
	{
		return self::construct($whFiled, 'NOT IN', '(' . implode(', ', array_fill(0, count($whValue), '?')) . ')');
	}

	public static function isNull($whFiled)
	{
		return self::construct($whFiled, 'IS NULL', ' ');
	}

	public static function isNotNull($whFiled)
	{
		return self::construct($whFiled, 'IS NOT NULL', ' ');
	}

	public static function eqy($whFiled, $whValue = null)
	{
		return self::construct($whFiled, '=', (!is_null($whValue) ? $whValue : ''));
	}

	public static function neq($whFiled, $whValue = null)
	{
		return self::construct($whFiled, '<>', (!is_null($whValue) ? $whValue : ''));
	}

	public static function lt($whFiled, $whValue = null)
	{
		return self::construct($whFiled, '<', (!is_null($whValue) ? $whValue : ''));
	}

	public static function lte($whFiled, $whValue = null)
	{
		return self::construct($whFiled, '<=', (!is_null($whValue) ? $whValue : ''));
	}

	public static function gt($whFiled, $whValue = null)
	{
		return self::construct($whFiled, '>', (!is_null($whValue) ? $whValue : ''));
	}

	public static function gte($whFiled, $whValue = null)
	{
		return self::construct($whFiled, '>=', (!is_null($whValue) ? $whValue : ''));
	}

	public static function construct($filed, $operator, $value = false)
	{
		return (!is_null(self::$type) ? ' ' . self::$type . ' ' : '') . $filed . ' ' . $operator . ' ' . ($value ? $value : '?');
	}
}