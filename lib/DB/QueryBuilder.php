<?

namespace DB;

use DB\Connection,
	DB\Helper\ExpressionBuilder;

class QueryBuilder
{
	const LIKE = 'like',
		  NOT_LIKE = 'notLike',
		  IN = 'in',
		  NOT_IN = 'notIn',
		  IS_NULL = 'isNull',
		  IS_NOT_NULL = 'isNotNull',
		  EQY = 'eqy',
		  NEQ = 'neq',
		  LT  = 'lt',
		  LTE = 'lte',
		  GT  = 'gt',
		  GTE = 'gte',
		  INNER_JOIN = 'INNER',
		  LEFT_OUTER_JOIN = 'LEFT OUTER',
		  RIGHT_OUTER_JOIN = 'RIGHT OUTER',
		  FULL_OUTER_JOIN = 'FULL OUTER',
		  ASC = 'ASC',
		  DESC = 'DESC',  
		  ERROR_SQL_WHERE = 'Значение %s не является приемлемым',
		  ERROR_CONST_OPERATOR = 'Оператор %s не является константой',
		  ERROR_JOIN = 'Ошибка построения JOIN соединения';

	protected $type,
			  $connection,
			  $params = [],
			  $_cache = [],
			  $alias = [],
			  $sqlP = [
				  'select' => [],
				  'table' => null,
				  'join' => [],
				  'where' => [],
				  'limit' => null,
				  'insert' => [],
				  'set' => [],
				  'orderBy' => [],
				  'groupBy' => null
			  ],
			  $order = [
				  'ASC',
				  'DESC'
			  ],
			  $joinWith = [
				  'INNER',
				  'LEFT OUTER',
				  'RIGHT OUTER',
				  'FULL OUTER'
			  ];

	public function __construct($config)
	{
		if(!is_array($config))
		{
			throw new \Exception('Ошибка подключения конфигурации');
		}

		$this->connection = new Connection($config);
	}

	public function add($sqlType, $sqlParams)
	{
		if(!isset($this->_cache['sqlP']))
		{
			$this->_cache['sqlP'] = $this->sqlP;
		}

		if(!is_array($sqlParams) && is_array($this->sqlP[$sqlType]))
		{
			$this->sqlP[$sqlType][] = $sqlParams;

			return $this;
		}

		$this->sqlP[$sqlType] = $sqlParams;

		return $this;
	}

	public function select(array $select = ['*'])
	{
		$this->type = 'SELECT';

		$this->add('select', $select);

		return $this;
	}

	public function from($table, $alias = null)
	{
		if(!is_null($alias))
		{
			$this->alias[$table] = $alias;
		}

		$this->add('table', $table);

		return $this;
	}

	public function join(array $join)
	{
		if(
			!isset($join[0])
			|| !isset($join[1])
			|| !in_array($join[2], $this->joinWith)
		)
		{
			$this->connection->exitConnectSql(null, self::ERROR_JOIN);

			return false;
		}

		$this->add('join', $join);

		return $this;
	}

	public function where(array $where)
	{
		$addWhere = $this->_constWhere($where);

		$this->add('where', $addWhere);

		return $this;
	}

	public function andWhere(array $where)
	{
		$andWhere = $this->getQueryPart('where');
		
		$andWhere = $this->_constWhere($where, ExpressionBuilder::TYPE_AND);

		$this->add('where', $andWhere);

		return $this;
	}

	public function orWhere(array $where)
	{
		$orWhere = $this->getQueryPart('where');
		
		$orWhere = $this->_constWhere($where, ExpressionBuilder::TYPE_OR);

		$this->add('where', $orWhere);

		return $this;
	}

	protected function _constWhere(array $where, $type = null)
	{
		$constWhere = null;

		$whOperator = $whValue = null;

		ExpressionBuilder::setType($type);

		if(key($where))
		{
			foreach($where as $key => $value)
			{
				$whFiled = $key;

				$whValue = $value;
			}

			if(
				!is_string($whFiled)
				|| empty($whValue)
			)
			{
				$this->connection->exitConnectSql($whFiled, self::ERROR_SQL_WHERE);
			}

			$constWhere = is_array($whValue) ? ExpressionBuilder::in($whFiled, $whValue) : ExpressionBuilder::eqy($whFiled);
		}
		else {
			if($this->isClassFunction('DB\Helper\ExpressionBuilder', $where[0]))
			{
				switch(count($where))
				{
					case 2:
						list($whOperator, $whFiled) = $where;
					break;

					case 3:
						list($whOperator, $whFiled, $whValue) = $where;
					break;

					default:
						$this->connection->exitConnectSql(null, self::ERROR_MYSQL_WHERE);
				}

				$constWhere = $this->getClassFunction('DB\Helper\ExpressionBuilder', $whOperator, [$whFiled, (is_array($whValue) ? $whValue : '')]);
			}
			else {
				$this->connection->exitConnectSql($where[0], self::ERROR_CONST_OPERATOR);
			}
		}

		$this->setParams($whValue);

		return $constWhere;
	}

	public function groupBy($colum)
	{
		if(!empty($colum))
		{
			return false;
		}

		$this->add('groupBy', $colum);

		return $this;
	}

	public function orderBy(array $order)
	{
		if(!in_array($order[1], $this->order))
		{
			return false;
		}

		$this->add('orderBy', $order);

		return $this;
	}

	public function limit($num)
	{
		if(!is_int($num))
		{
			return false;
		}

		$this->add('limit', $num);

		return $this;
	}

	public function insert(array $ins)
	{
		if(
			!empty($ins)
			|| !key($ins)
		)
		{
			return false;
		}

		$this->add('insert', $ins);

		return $this;
	}

	public function into($table)
	{
		if(empty($talbe))
		{
			$this->type = 'INSERT';

			$this->add('table', $table);

			return $this;
		}

		return false;
	}

	public function update($table)
	{
		if(empty($talbe))
		{
			$this->type = 'UPDATE';

			$this->add('table', $table);

			return $this;
		}

		return false;
	}

	public function set(array $kv)
	{
		$isKey = $isValue = [];

		foreach($kv as $key => $value)
		{
			$set[] = $key . ' = ?';
		}

		$this->setParams(array_values($kv));

		$this->add('set', $set);

		return $this;
	}

	public function delete($table)
	{
		if(empty($talbe))
		{
			$this->type = 'DELETE';

			$this->add('table', $table);

			return $this;
		}

		return false;
	}

	private function setParams($key)
	{
		if(!is_null($key)) {
			if(is_array($key))
			{
				foreach($key as $v)
				{
					$this->params[] = $v;
				}
			}
			else {
				$this->params[] = $key;
			}
		}
	}

	private function getQueryPart($queryName)
	{
		return $this->sqlP[$queryName];
	}

	private function getSql()
	{
		switch($this->type)
		{
			case 'SELECT':
				$query = $this->getSelectQuery();
			break;

			case 'INSERT':
				$query = $this->getInsertQuery();
			break;

			case 'UPDATE':
				$query = $this->getUpdateQuery();
			break;

			case 'DELETE':
				$query = $this->getDeleteQuery();
			break;

			default:
				die('SQL Error');
		}

		return $query;
	}

	private function getSelectQuery()
	{
		$query = 'SELECT '
			   . implode(', ', $this->getColumAs())
			   . ' FROM ' . (isset($this->alias[$this->sqlP['table']]) ? $this->sqlP['table'] . ' ' . $this->alias[$this->sqlP['table']] : $this->sqlP['table'])
			   . (array_values($this->sqlP['join']) ? $this->getJoinQuery($this->sqlP['join']) : '')
			   . (array_values($this->sqlP['where']) ? ' WHERE ' . implode('', $this->sqlP['where']) : '')
			   . (!is_null($this->sqlP['groupBy']) ? ' GROUP BY ' . $this->sqlP['groupBy'] : '')
			   . (array_values($this->sqlP['orderBy']) ? ' ORDER BY ' . implode(' ', $this->sqlP['orderBy']) : '')
			   . (!is_null($this->sqlP['limit']) ? ' LIMIT ' . ((int) $this->sqlP['limit']) : '');

		return $query;

	}

	private function getInsertQuery()
	{
		$this->setParams(array_values($this->sqlP['insert']));

		$query = 'INSERT INTO '
			. $this->sqlP['table']
			. ' (' . implode(', ', array_keys($this->sqlP['insert'])) . ') VALUES (' . implode(', ', array_fill(0, count($this->sqlP['insert']), '?')) . ')';

		return $query;
	}

	private function getUpdateQuery()
	{
		$query = 'UPDATE '
			. $this->sqlP['table']
			. ' SET '
			. implode(', ', $this->sqlP['set'])
			. (array_values($this->sqlP['where']) ? ' WHERE ' . implode('', $this->sqlP['where']) : '');

		return $query;
	}

	private function getColumAs()
	{
		if(key($this->sqlP['select']))
		{
			foreach($this->sqlP['select'] as $key => $value)
			{
				$select[] = ($key ? $key . ' ' . $value : $value);
			}

			return $select;
		}

		return $this->sqlP['select'];
	}

	private function getDeleteQuery()
	{
		$query = 'DELETE FROM '
			. $this->sqlP['table']
			. (array_values($this->sqlP['where']) ? ' WHERE ' . implode('', $this->sqlP['where']) : '');

		return $query;
	}

	private function getJoinQuery()
	{
		$join = $this->sqlP['join'];

		if(array_key_exists(3, $join))
		{
			list($jTable, $jAlias, $jWith, $jWhParametrs) = $join;

			if($this->isClassFunction('DB\Helper\ExpressionBuilder', $jWhParametrs[0]))
			{
				$join = ' ' . $jWith . ' ' . 'JOIN ' . $jTable . ' ' . $jAlias . ' ON '. $this->getClassFunction('DB\Helper\ExpressionBuilder', $jWhParametrs[0], [$jWhParametrs[1], (array_key_exists(2, $jWhParametrs) ? $jWhParametrs[2] : null)]);

				return $join;
			}
		}
		else {
			list($jTable, $jAlias, $jWith) = $join;

			$join = ' ' . $jWith . ' ' . 'JOIN ' . $jTable . ' ' . $jAlias;

			return $join;
		}
	}

	public function execute()
	{
		$executeQuery = $this->connection->executeQuery($this->getSql(), $this->params, $this->type);

		$this->clearQuery();

		return $executeQuery;
	}

	private function clearQuery()
	{
		$this->sqlP = $this->_cache['sqlP'];

		$this->params = $this->type = null;
	}

	public function rawSql()
	{
		echo $this->connection->rawSql();
	}

	private function isClassFunction($class, $function)
	{
		if(is_callable([$class, $function]))
		{
			return true;
		}

		return false;
	}

	private function getClassFunction($class, $function, array $params)
	{
		return call_user_func_array([$class, $function], $params);
	}
}