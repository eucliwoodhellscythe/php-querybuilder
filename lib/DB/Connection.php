<?

namespace DB;

use PDO;

class Connection
{
	protected $pdo,
			  $config,
			  $isConnecting = true,
			  $_cache,
			  $stmt;

	public function __construct($config)
	{
		$this->config = $config;
	}

	private function connect()
	{
		if($this->isConnecting) {
			try {
				$this->pdo = new PDO($this->config['db']['driver'].":host=".$this->config['db']['host'].";dbname=".$this->config['db']['table'], $this->config['db']['user'], $this->config['db']['password']);

				$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

				return true;
			}
			catch(PDOException $e) {
				echo $e->getMessage();  
			}
		}
		else {
			return false;
		}
	}

	public function exitConnectSql($field, $message)
	{
		$this->pdo = null;
		$this->isConnecting = false;

		$this->flushErrorMessage($field, $message);
	}

	private function flushErrorMessage($field, $message)
	{
		die(sprintf($message, '<b>' . $field . '</b>'));
	}

	private function setCahceParams($query, $params, $type, $result)
	{
		$this->set(['query' => $query, 'params' => $params, 'result' => $result, 'type' => $type]);
	}

	private function set($olg)
	{
		if(is_array($olg) || key($olg))
		{
			$this->_cache = $olg;
		}

		return false;
	}

	public function executeQuery($query, $params = [], $type)
	{
		$connect = $this->connect();

		if($connect)
		{
			$this->pdo->beginTransaction();

			$queryFunc = call_user_func_array([$this, mb_strtolower($type)], [$query, $params]);

			$this->pdo->commit();

			$this->setCahceParams($query, $params, $type, ($type == 'SELECT' ? $queryFunc->fetchAll() : $queryFunc));

			/*if(!$queryFunc)
			{
				return print_r($queryFunc->errorInfo());
			}*/

			return $queryFunc;
		}
	}

	private function select($query, array $params)
	{
		$stmt = $this->pdo->prepare($query);

		$stmt->execute($params);

		return $stmt;
	}

	private function update($query, array $params)
	{
		$stmt = $this->pdo->prepare($query);

		$stmt->execute($params);

		return $stmt->rowCount();
	}

	private function insert($query, array $params)
	{
		$stmt = $this->pdo->prepare($query);

		$stmt->execute($params);

		return $this->pdo->lastInsertId();
	}

	private function delete($query, array $params)
	{
		if(array_values($params))
		{
			foreach($params as $param)
			{
				$paramsQuote[] = $this->pdo->quote($param);
			}

			$query = str_replace(array_fill(0, count($params), '?'), $paramsQuote, $query);
		}

		return $this->pdo->exec($query);
	}

	public function rawSql()
	{
		echo '<pre>';
			print_r($this->_cache);
		echo '</pre>';
	}
}