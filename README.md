```
#!php

require '/config/local.config.php';
```



```
#!php

$db = new QueryBuilder($config);
```

## select ##:
 - ### select(array $select = ['*']) ###
     - $select может быть исключительно массивом. Если в функцию select() ни чего не передать, то $select примет автоматически массив = ['*']
 - ### from($table, $alias = null) ###
     - $table - наименование таблицы, $alias псевдоним таблицы, по улномочию принимает значение null
 - where(array $where)
     - $select - должен быть передан в виде массива, есть 2 метода построения выражения:
     1. Передача массива ввиде Ключ + Значение: ->where(['id' => '1']) => WHERE id = 1
     2. Передача массива, где 1 значение - Константа: ->where([QueryBuilder::EQY, 'id', '1']), где 1 значение - Константа, 2 - Наименование колонки, 3  => WHERE id = 1. В случае, когда вы используете константы ::IN, ::NOT_IN, 3 значение трубет передавать в виде массива значений. Если вы использует константы, такие как как ::IS_NOT_NULL, ::IS_NULL, нет небходимости прописывать в массиве 3 значение.
     - Список констант и их выражения для 2 метода:
       * QueryBuilder::LIKE => field LIKE value
       * QueryBuilder::NOT_LIKE => field NOT LIKE value
       * QueryBuilder::IN => field IN (value, value1)
       * QueryBuilder::NOT_IN => field NOT IN (value, value1)
       * QueryBuilder::IS_NULL => field IS NULL
       * QueryBuilder::IS_NOT_NULL => field IS NOT NULL
       * QueryBuilder::EQY => field = value
       * QueryBuilder::NEQ => field <> value
       * QueryBuilder::LT => field < value
       * QueryBuilder::LTE  => field <= value
       * QueryBuilder::GT => field > value
       * QueryBuilder::GTE => field >= value
 - ### andWhere(array $andWhere) и orWhere(array $orWhere) ###
     - Также, как и where принимают аналогичные построения выражений;
 - ### join(array $join) ###
     - Массив $join должен содержать в себе следующие значения: ->join(['Имя таблицы', 'Псевдноним', 'With', 'Выражение']);
     - With передаётся в виде константы, список констант:
     * QueryBuilder::INNER_JOIN
     * QueryBuilder::LEFT_OUTER_JOIN
     * QueryBuilder::RIGHT_OUTER_JOIN
     * QueryBuilder::FULL_OUTER_JOIN
 - ### orderBy(array $order) ###
     - $order массив, содержащий в себе 2 значения, 1 - наименование колонки, 2 - тип сортировки (QueryBuilder::ASC или QueryBuilder::DESC)
 - ### groupBy($colum) ###
     - $colum - наименование колонки для группировки значений
 - ### limit($num) ###
     - $num - принимает только числовые значения

## insert ##:
 - ### insert(array $ins) ###
     - $ins должен быть исключительно массивом, который принимает Ключ + Значение, где ключ - наименование колонки.
 - ### into($table) ###
     - $table - наименование таблицы, в которую будут внесены новые данные

## update ##:
 - ### update($table) ###
     - $table наименование таблицы, в которой будет производиться обновление
 - ### set(array $set) ###
     - $set должен быть исключительно массивом, который принимает Ключ + Значение, где ключ - наименование колонки.
 - ### where($where) ###
     - Выражение, в котором указываем, по каким критериям будет искать и обновлять строку.

## delete ##:
 - ### delete($table) ###
     - $table наименование таблицы, в которой будет удалять данные;
 - ### where($where) ###
     - Выражение, в котором указываем, по каким критериям будет искать и удалять строку.